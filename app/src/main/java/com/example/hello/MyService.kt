package net.simplifiedcoding.androidserviceexample

import android.app.Service
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlin.concurrent.fixedRateTimer


/**
 * Created by Belal on 12/30/2016.
 */
class MyService : Service() {
    //creating a mediaplayer object
    //private lateinit var player: MediaPlayer
    //val database = Firebase.database
   // var count= 1000
    val timer = java.util.Timer()
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }


    override fun onStartCommand(
        intent: Intent?,
        flags: Int,
        startId: Int
    ): Int { //getting systems default ringtone
        super.onStartCommand(intent, flags, startId);
        Log.i("LocalService", "Received start id " + startId + ": " + intent);

        val database = FirebaseDatabase.getInstance()

        Toast.makeText(this, "test", Toast.LENGTH_SHORT).show()
        //Log.i("LocalService", "Received start id " +  myRef.getValue() + ": " + intent);
        /*val task = object: TimerTask() {
            var timesRan = 0
            override fun run() { Toast.makeText(this, "testing", Toast.LENGTH_SHORT).show() }
        }
        timer.schedule(task, 0, 5000)*/

        fixedRateTimer("timer",false,0,10000){
            val prefs =
                getSharedPreferences("prefs", Context.MODE_PRIVATE)
            var count = prefs.getInt("count",1000)

            val myRef = database.getReference("message")
            myRef.setValue(count);
            myRef.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.
                    var value = dataSnapshot.getValue(Int::class.java)
                    Log.d(TAG, "Value is: $value")
                    if(value!= null){value = value - 1}

                    val prefs = getSharedPreferences("prefs", Context.MODE_PRIVATE)
                    val editor = prefs.edit()
                    if(value!= null){ editor.putInt("count", value.toInt())}
                    editor.apply()
                }

                override fun onCancelled(error: DatabaseError) {
                    // Failed to read value
                    Log.w(TAG, "Failed to read value.", error.toException())
                }

            })




        }


        /*player = MediaPlayer.create(
            this,
            Settings.System.DEFAULT_RINGTONE_URI
        )

        //setting loop play to true
//this will make the ringtone continuously playing
        player.setLooping(true)
        //staring the player
        player.start()*/
        //we have some options for service
//start sticky means service will be explicity started and stopped
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        fixedRateTimer("timer",false,0,0){

            Log.i("LocalService", " start id ");

        }
        //stopping the player when service is destroyed
       // player!!.stop()
    }
}
