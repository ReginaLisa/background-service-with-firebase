package com.example.hello

//import android.support.v7.app.AppCompatActivity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import net.simplifiedcoding.androidserviceexample.MyService



class MainActivity : AppCompatActivity(), View.OnClickListener {
    //button objects
    private var buttonStart: Button? = null
    private var buttonStop: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //getting buttons from xml
        buttonStart = findViewById<View>(R.id.buttonStart) as Button
        buttonStop = findViewById<View>(R.id.buttonStop) as Button
        //attaching onclicklistener to buttons
        buttonStart!!.setOnClickListener(this)
        buttonStop!!.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        if (view === buttonStart) { //starting service
           // Toast.makeText(this, "test", Toast.LENGTH_SHORT).show()
            Log.i("LocalService", " start id ");
           // startService(Intent(this, MyService::class.java))
            startService(Intent(this@MainActivity, MyService::class.java))
        } else if (view === buttonStop) { //stopping service
            stopService(Intent(this, MyService::class.java))
        }
    }
}