package com.example.hello

import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
//import android.support.v7.app.AppCompatActivity
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.TextView
//import androidx.appcompat.app.AppCompatActivity
import java.util.*

class MainActivity : AppCompatActivity() {
    private var mTextViewCountDown: TextView? = null
    private lateinit var mButtonStartPause: Button
    private lateinit var mButtonReset: Button
    private var mCountDownTimer: CountDownTimer? = null
    private var mTimerRunning = false
    private var mTimeLeftInMillis: Long = 0
    private var mEndTime: Long = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mTextViewCountDown = findViewById(R.id.text_view_countdown)
        mButtonStartPause = findViewById(R.id.button_start_pause)
        mButtonReset = findViewById(R.id.button_reset)
        mButtonStartPause.setOnClickListener(View.OnClickListener {
            if (mTimerRunning) {
                pauseTimer()
            } else {
                startTimer()
            }
        })
        mButtonReset.setOnClickListener(View.OnClickListener { resetTimer() })

    }

    private fun startTimer() {
        mEndTime = System.currentTimeMillis() + mTimeLeftInMillis
        mCountDownTimer = object : CountDownTimer(mTimeLeftInMillis, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                mTimeLeftInMillis = millisUntilFinished
                updateCountDownText()
            }

            override fun onFinish() {
                mTimerRunning = false
                updateButtons()
            }
        }.start()
        mTimerRunning = true
        updateButtons()
    }

    private fun pauseTimer() {
        mCountDownTimer!!.cancel()
        mTimerRunning = false
        updateButtons()
    }

    private fun resetTimer() {
        mTimeLeftInMillis = START_TIME_IN_MILLIS
        updateCountDownText()
        updateButtons()
    }

    private fun updateCountDownText() {
        val minutes = (mTimeLeftInMillis / 1000).toInt() / 60
        val seconds = (mTimeLeftInMillis / 1000).toInt() % 60
        val timeLeftFormatted =
            String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds)
        mTextViewCountDown!!.text = timeLeftFormatted
    }

    private fun updateButtons() {
        if (mTimerRunning) {
            mButtonReset!!.visibility = View.INVISIBLE
            mButtonStartPause!!.text = "Pause"
        } else {
            mButtonStartPause!!.text = "Start"
            if (mTimeLeftInMillis < 1000) {
                mButtonStartPause!!.visibility = View.INVISIBLE
            } else {
                mButtonStartPause!!.visibility = View.VISIBLE
            }
            if (mTimeLeftInMillis < START_TIME_IN_MILLIS) {
                mButtonReset!!.visibility = View.VISIBLE
            } else {
                mButtonReset!!.visibility = View.INVISIBLE
            }
        }
    }

    override fun onStop() {
        super.onStop()
        val prefs =
            getSharedPreferences("prefs", Context.MODE_PRIVATE)
        val editor = prefs.edit()
        editor.putLong("millisLeft", mTimeLeftInMillis)
        editor.putBoolean("timerRunning", mTimerRunning)
        editor.putLong("endTime", mEndTime)
        editor.apply()
        if (mCountDownTimer != null) {
            mCountDownTimer!!.cancel()
        }
    }

    override fun onStart() {
        super.onStart()
        val prefs =
            getSharedPreferences("prefs", Context.MODE_PRIVATE)
        mTimeLeftInMillis = prefs.getLong("millisLeft", START_TIME_IN_MILLIS)
        mTimerRunning = prefs.getBoolean("timerRunning", false)
        updateCountDownText()
        updateButtons()
        if (mTimerRunning) {
            mEndTime = prefs.getLong("endTime", 0)
            mTimeLeftInMillis = mEndTime - System.currentTimeMillis()
            if (mTimeLeftInMillis < 0) {
                mTimeLeftInMillis = 0
                mTimerRunning = false
                updateCountDownText()
                updateButtons()
            } else {
                startTimer()
            }
        }
    }

    companion object {
        private const val START_TIME_IN_MILLIS: Long = 600000
    }
}